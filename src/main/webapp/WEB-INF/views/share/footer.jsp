<div class="container footer">

    <hr>

    <!-- Footer -->
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; Food Panda 2021 - Designed and
                    Developed By Gurpreet & Ramakrishnan</p>
            </div>
        </div>
    </footer>

</div>