package com.capstone.foodpanda.service;

import com.capstone.foodpanda.model.User;

public interface UserService {

    boolean saveUser(User user);

    User findUserByEmail(String email);

}
