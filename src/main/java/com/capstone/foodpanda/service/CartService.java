package com.capstone.foodpanda.service;

import com.capstone.foodpanda.model.Cart;

public interface CartService {

    boolean saveCart(Cart cart);

    boolean updateCart(Cart cart);

    Cart findCart();

}
