package com.capstone.foodpanda.service;

import com.capstone.foodpanda.model.Address;

public interface AddressService {

    boolean saveAddress(Address address);

    Address findAddressByBilling(boolean billing);

}
