package com.capstone.foodpanda.controllers;

import com.capstone.foodpanda.model.Cart;
import com.capstone.foodpanda.model.CartLine;
import com.capstone.foodpanda.model.Product;
import com.capstone.foodpanda.service.CartLineService;
import com.capstone.foodpanda.service.CartService;
import com.capstone.foodpanda.service.ProductService;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;
import org.junit.Test;
import org.junit.Assert;

public class CartControllerTest {

    @Test
    public void showCartUpdatedTest() {
        CartController cartController = new CartController();
        ModelAndView mvc = cartController.showCart("updated");
        Assert.assertEquals("title", mvc.getViewName());
    }

    @Test
    public void showCartErrorTest() {
        CartController cartController = new CartController();
        ModelAndView mvc = cartController.showCart("error");
        Assert.assertEquals("title", mvc.getViewName());
    }

    @Test
    public void showCartAddedTest() {
        CartController cartController = new CartController();
        ModelAndView mvc = cartController.showCart("added");
        Assert.assertEquals("title", mvc.getViewName());
    }

    @Test
    public void showCartDeletedTest() {
        CartController cartController = new CartController();
        ModelAndView mvc = cartController.showCart("deleted");
        Assert.assertEquals("title", mvc.getViewName());
    }

    @Test
    public void updateCartErrorTest() {
        CartController cartController = new CartController();
        String abc = cartController.updateCart(1, 1);
        Assert.assertEquals("redirect:/cart/show?result=error", abc);
    }

    @Test
    public void deleteCartErrorTest() {
        CartController cartController = new CartController();
        String abc = cartController.deleteCart(1);
        Assert.assertEquals("redirect:/cart/show?result=error", abc);
    }

    @Test
    public void addCartTest() {
        CartController cartController = new CartController();
        String abc = cartController.addCart(1);
        Assert.assertEquals("redirect:/cart/show?result=added", abc);
    }
}
